const PriceTable = require("./price-table.json")

type TItemNames = "Apple" | "Pear" | "Pringles" | "Haribo Gummy Bear" | "Coca Cola" | "Sprite"

interface IResults {
  [x: string]: {
    total: number,
    price: number
  }
}

interface IDiscount {
  [x: string]: {
    minQty: number,
    amount: number
  }
}

const mockDiscountInput: IDiscount = {
  Apple: {
    minQty: 3,
    amount: 0.2
  },
  Pear: {
    minQty: 1,
    amount: 0.3
  },
}

function priceFormatter(price: number): number {
  return Number(price.toFixed(2))
}

function applyDiscount(pickedItems: IResults, discountRate: IDiscount) {
  const itemsWasDiscounted: IResults = {}
  for (let i in pickedItems) {
    if (discountRate[i] && pickedItems[i] && pickedItems[i].total >= discountRate[i].minQty) {
      itemsWasDiscounted[i] = {
        total: pickedItems[i].total,
        price: priceFormatter(pickedItems[i].price * (1 - discountRate[i].amount))
      }
    } else {
      itemsWasDiscounted[i] = pickedItems[i]
    }
  }
  return itemsWasDiscounted
}

function pricePrinter(
  itemNames: TItemNames[],
  isApplyDiscount: boolean = false,
  discountRate: IDiscount = {}
): string {

  let finalResults = itemNames.reduce((results: IResults, i: TItemNames) => {
    if (PriceTable[i]) {
      if (results && !results[i]) {
        results[i] = {
          total: 1,
          price: priceFormatter(PriceTable[i])
        }
      } else {
        results[i].total += 1
        const newTotalPriceItem = results[i]!.total * PriceTable[i]
        results[i].price = priceFormatter(newTotalPriceItem)
      }
    }
    return results
  }, {})

  if (isApplyDiscount && discountRate) {
    finalResults = applyDiscount(finalResults, discountRate)
  }

  let finalString: string = ""
  let total: number = 0
  for (let i in finalResults) {
    const eachItemString = `${finalResults[i].total}x ${i} $${PriceTable[i]} \n`
    finalString += eachItemString
    total += priceFormatter(finalResults[i].price)
  }
  finalString += "----------------- \n"
  finalString += `Total $${total}`

  console.log(finalString)
  return finalString
}

export {
  priceFormatter,
  applyDiscount,
  pricePrinter,
  mockDiscountInput,
  TItemNames
}