"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = require("./index");
var mockInput = ["Apple", "Apple", "Apple", "Apple", "Pear", "Pringles"];
var mockResultWithoutDiscount = "4x Apple $1 \n1x Pear $1.2 \n1x Pringles $4.2 \n----------------- \nTotal $9.4";
var mockResultWithDiscount = "4x Apple $1 \n1x Pear $1.2 \n1x Pringles $4.2 \n----------------- \nTotal $8.24";
var mockPickedItems = {
    Apple: {
        total: 4,
        price: 4
    },
    Pear: {
        total: 1,
        price: 1.2
    },
};
var mockPickedItemsAfterDiscount = {
    Apple: {
        total: 4,
        price: 3.2
    },
    Pear: {
        total: 1,
        price: 0.84
    },
};
describe("test pricePrinter", function () {
    it("test price formatter", function () {
        expect(index_1.priceFormatter(4.002)).toEqual(4.00);
    });
    it("test applyDiscount", function () {
        expect(JSON.stringify(index_1.applyDiscount(mockPickedItems, index_1.mockDiscountInput))).toEqual(JSON.stringify(mockPickedItemsAfterDiscount));
    });
    it("test pricePrinter function without discount", function () {
        expect(index_1.pricePrinter(mockInput, false)).toBe(mockResultWithoutDiscount);
    });
    it("test pricePrinter function with discount", function () {
        expect(index_1.pricePrinter(mockInput, true, index_1.mockDiscountInput)).toBe(mockResultWithDiscount);
    });
});
