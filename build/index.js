"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mockDiscountInput = exports.pricePrinter = exports.applyDiscount = exports.priceFormatter = void 0;
var PriceTable = require("./price-table.json");
var mockDiscountInput = {
    Apple: {
        minQty: 3,
        amount: 0.2
    },
    Pear: {
        minQty: 1,
        amount: 0.3
    },
};
exports.mockDiscountInput = mockDiscountInput;
function priceFormatter(price) {
    return Number(price.toFixed(2));
}
exports.priceFormatter = priceFormatter;
function applyDiscount(pickedItems, discountRate) {
    var itemsWasDiscounted = {};
    for (var i in pickedItems) {
        if (discountRate[i] && pickedItems[i] && pickedItems[i].total >= discountRate[i].minQty) {
            itemsWasDiscounted[i] = {
                total: pickedItems[i].total,
                price: priceFormatter(pickedItems[i].price * (1 - discountRate[i].amount))
            };
        }
        else {
            itemsWasDiscounted[i] = pickedItems[i];
        }
    }
    return itemsWasDiscounted;
}
exports.applyDiscount = applyDiscount;
function pricePrinter(itemNames, isApplyDiscount, discountRate) {
    if (isApplyDiscount === void 0) { isApplyDiscount = false; }
    if (discountRate === void 0) { discountRate = {}; }
    var finalResults = itemNames.reduce(function (results, i) {
        if (PriceTable[i]) {
            if (results && !results[i]) {
                results[i] = {
                    total: 1,
                    price: priceFormatter(PriceTable[i])
                };
            }
            else {
                results[i].total += 1;
                var newTotalPriceItem = results[i].total * PriceTable[i];
                results[i].price = priceFormatter(newTotalPriceItem);
            }
        }
        return results;
    }, {});
    if (isApplyDiscount && discountRate) {
        finalResults = applyDiscount(finalResults, discountRate);
    }
    var finalString = "";
    var total = 0;
    for (var i in finalResults) {
        var eachItemString = finalResults[i].total + "x " + i + " $" + PriceTable[i] + " \n";
        finalString += eachItemString;
        total += priceFormatter(finalResults[i].price);
    }
    finalString += "----------------- \n";
    finalString += "Total $" + total;
    console.log(finalString);
    return finalString;
}
exports.pricePrinter = pricePrinter;
