import {pricePrinter, applyDiscount, priceFormatter, TItemNames, mockDiscountInput} from "./index"

const mockInput: TItemNames[] = ["Apple", "Apple", "Apple", "Apple", "Pear", "Pringles"]

const mockResultWithoutDiscount =
  "4x Apple $1 \n1x Pear $1.2 \n1x Pringles $4.2 \n----------------- \nTotal $9.4"

const mockResultWithDiscount =
  "4x Apple $1 \n1x Pear $1.2 \n1x Pringles $4.2 \n----------------- \nTotal $8.24"

const mockPickedItems = {
  Apple: {
    total: 4,
    price: 4
  },
  Pear: {
    total: 1,
    price: 1.2
  },
}

const mockPickedItemsAfterDiscount = {
  Apple: {
    total: 4,
    price: 3.2
  },
  Pear: {
    total: 1,
    price: 0.84
  },
}

describe("test pricePrinter", () => {

  it("test price formatter", () => {
    expect(priceFormatter(4.002)).toEqual(4.00)
  })

  it("test applyDiscount", () => {
    expect(JSON.stringify(applyDiscount(mockPickedItems, mockDiscountInput))).toEqual(JSON.stringify(mockPickedItemsAfterDiscount))
  })

  it("test pricePrinter function without discount", () => {
    expect(pricePrinter(mockInput, false)).toBe(mockResultWithoutDiscount)
  })

  it("test pricePrinter function with discount", () => {
    expect(pricePrinter(mockInput, true, mockDiscountInput)).toBe(mockResultWithDiscount)
  })
})



